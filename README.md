# StockItAll

StockItAll is a fifictive company that a wants a Inventory system writen in python.
The application should contain a server with the possibility to save and update and return one or every item that is stored in the "Database".

The server will also contain an CLI thread that makes it possible to access the data from the CLI.
The CLI should be able to add, update and list all items.

They also wants a GUI to use remotly from the server, this app should also be able to add, update and list items stored in the database.

* Main.py script for
    * displaying the CLI menu,
    * accessing the StockTracker
    * storing and retrieving the StockTracker
    * performing threading
    * running a server
* A Client.py script for
    * creating a GUI-based interface
    * capturing a StockItem’s code, description and amount
    * acting as a client to the server
    * displaying a messagebox
* Your code needs to be fully commented and include docstrings