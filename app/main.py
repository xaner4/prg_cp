#!/usr/bin/python3
import tkinter as tk
import socket
import logging
import json
import ast

from config import *

logging.basicConfig(filename='app.log', filemode='a', format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.DEBUG)

class Connection:
    def __init__(self, IPAddress, port):
        self.IPAddress = IPAddress
        self.port = port
        self.headersize = 16

    def openConnection(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.IPAddress, self.port))
            logging.info(f"Connected to {IP} on Port: {PORT}")

    # 1001 = New, 1002 = Edit, 1003 = Get spesific ID, 1004 = Get all items
    def sendCMD(self, code, msg=""):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.IPAddress, self.port))
            cmdMsg = f"{len(code+msg):<{self.headersize}}" + code + msg
            logging.debug(f"Sending {cmdMsg} to server")
            s.send(bytes(cmdMsg,"utf-8"))
            logging.info(f"CMD Sendt to Server")
            data = self.getData(s)
            return data

    def getData(self, s):
        full_msg =  ""
        new_msg = True
        while True:
            cmd = s.recv(self.headersize)
            if new_msg:
                cmdlen = int(cmd[:self.headersize])
                new_msg = False

            full_msg += cmd.decode("utf-8")
            
            if len(full_msg)-self.headersize == cmdlen:
                logging.debug(f"Reciving from server{full_msg}")
                logging.info("New Command Recived")
                full_msg = full_msg[self.headersize:]
                return full_msg


class Application(tk.Frame):
    def __init__(self, master):
        super().__init__(master)
        master.title("Stock Tracker")
        master.geometry("700x350")
        self.create_widgets()
        self.populateList()

    def create_widgets(self):
        logging.info("Drawing Window")
        # ID field
        self.id_text = tk.StringVar()
        self.id_label = tk.Label(
            self.master, text="Item ID", pady=20, font=('bold', 14)
        )
        self.id_label.grid(row=0, column=0, sticky=tk.W)
        self.id_entry = tk.Entry(self.master, textvariable=self.id_text)
        self.id_entry.grid(row=0, column=1)

        # Name Field
        self.name_text = tk.StringVar()
        self.name_label = tk.Label(
            self.master, text="Item name", font=('bold', 14)
        )
        self.name_label.grid(row=0, column=2, sticky=tk.W)
        self.name_entry = tk.Entry(self.master, textvariable=self.name_text)
        self.name_entry.grid(row=0, column=3)

        # Description Field
        self.description_text = tk.StringVar()
        self.description_label = tk.Label(
            self.master, text="Item description", font=('bold', 14)
        )
        self.description_label.grid(row=1, column=0, sticky=tk.W)
        self.description_entry = tk.Entry(self.master, textvariable=self.description_text)
        self.description_entry.grid(row=1, column=1)

        # Amount Field
        self.amount_text = tk.StringVar()
        self.amount_label = tk.Label(
            self.master, text="Item amount", font=('bold', 14)
        )
        self.amount_label.grid(row=1, column=2, sticky=tk.W)
        self.amount_entry = tk.Entry(self.master, textvariable=self.amount_text)
        self.amount_entry.grid(row=1, column=3)

        # Item List
        self.item_list = tk.Listbox(self.master, height=8, width=50, border=1)
        self.item_list.grid(row=3, column=0, columnspan=3,
                             rowspan=6, pady=20, padx=20)
        self.scrollbar = tk.Scrollbar(self.master)
        self.scrollbar.grid(row=3, column=3)
        self.item_list.configure(yscrollcommand=self.scrollbar.set)
        self.scrollbar.configure(command=self.item_list.yview)
        
        # Add Item buttun
        self.add_btn = tk.Button(
            self.master, text="Add Item", width=12, command=self.addItem
            )
        self.add_btn.grid(row=2, column=0, pady=20)

        # Update Item Button
        self.update_btn = tk.Button(
            self.master, text="Update Item", width=12, command=self.updateItem
            )
        self.update_btn.grid(row=2, column=1)
        
        # 
        self.exit_btn = tk.Button(
            self.master, text="Clear Input", width=12, command=self.clearText
            )
        self.exit_btn.grid(row=2, column=3)
        
        # ID: {item["id"]}, Name: {item["name"]} Description: {item["description"]}, Amount: {item["amount"]}

    def populateList(self):
        inventory = connection.sendCMD("1004")
        inventory = ast.literal_eval(inventory)
        logging.debug(inventory)
        for item in inventory.items():
            self.item_list.insert(tk.END, f"{item}")

    def addItem(self):
        if self.name_text.get() == "" or self.description_text.get() == "" or self.amount_text.get() == "":
            tk.messagebox.showerror("One or more fields is empty, Please include all fields")
        
        d = {
            "name": self.name_text.get(),
            "description": self.description_text.get(),
            "amount": self.amount_text.get()
        }
        connection.sendCMD("1001", f"name: {self.name_text.get()}, self.description_text.get(), self.amount_text.get()")
        

    def updateItem(self):
        pass

    def clearText(self):
        self.id_entry.delete(0, tk.END)
        self.name_entry.delete(0, tk.END)
        self.description_entry.delete(0, tk.END)
        self.amount_entry.delete(0, tk.END)


try:
    connection = Connection(IP, PORT)
except ConnectionRefusedError:
    logging.warn(f"Could not connet to {IP}:{PORT}")
root = tk.Tk()
app = Application(master=root)
app.mainloop()