#!/usr/bin/python3
import os
import json
import logging

class StockItem:
    """
        Arguments:
            _id {str} -- [used only when editing]
            name {str} -- [Name of the item ]
            description {str} -- [A description of the Item]
            amount {int} -- [How many of the current item]
    
    """
    def __init__(self,_id: str, name: str, description: str, amount: int):
        self.id = _id
        self.name = name
        self.description = description
        self.amount = amount
        self.item = dict()
    
    def addNewItem(self) -> dict:
        """
        Gets the input from both server and the CLI and structure the data befor something more is done with the data

        Returns:
            dict -- Returns the right structure of the data frame
        """
        
        self.item = {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "amount": self.amount
        }
        return self.item

    def updateItem(self) -> dict:
        """
        Updat an item in the Inventory        

        # TODO: Get the last stored data, check if the input was empty and save the old data instead of empty.

        Returns:
            dict -- Retruns the new structure for the edited item
        """

        self.item = {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "amount": self.amount
        }
        return self.item
 

class StockTracker:
    """
    Controll everything that have to do with the database
    """
    def __init__(self):
        self._inventory = dict()
        self.dbpath = self._dbpath()
        self.dbfile = os.path.join(self.dbpath, "inventory.json")
        self._startup()

    @property
    def inventory(self):
        return self._inventory

    @inventory.setter
    def inventory(self, value):
        self._inventory = value

    def _dbpath(self):
        """FInd the relative path to where the Database file will be saved
        
        Returns:
            str -- the relative path to the database file
        """
        dirname = os.path.dirname(__file__)
        dbpath = "./db/"
        fullpath = os.path.join(dirname, dbpath)
        return fullpath

    def _startup(self):
        """
            Check if the database path exsist if not make it
        """
        if not os.path.exists(self.dbpath):
            os.makedirs(self.dbpath)
            logging.info(f"Database dir was made {self.dbpath}")
        if not os.path.isfile(self.dbfile):
            with open(self.dbfile, "w+") as f:
                logging.info(f"db file was made {self.dbpath}")

    def loadDB(self):
        """ 
            Checks if the dbfile is not empty and if it is not empty it will load the json file into self.inventory
        """
        with open(self.dbfile, "r") as file:
            if os.stat(self.dbfile).st_size ==  0:
                logging.info("DB file is empty")
            else:
                self.inventory = json.load(file)
                logging.info("Load DB into mem")

    def saveDB(self):
        """ 
            Takes "self.inventory" and overwrite the "self.dbfile"
        """
        with open(self.dbfile, "w+") as file:
            json.dump(self.inventory, file,indent=2)
            logging.info("DB written to file")
    
    def getItem(self, _id: str = None, name: str = None) -> dict:
        """
        Looks through self.inventory dictonary and find item based on id or name
        
        Keyword Arguments:
            _id {str} -- [return item based on ID] (default: {None})
            name {str} -- [retrun item based on name] (default: {None})
        
        Raises:
            KeyError: No item was found with ID
            KeyError: No item was found with name
            ValueError: No argument was given
        
        Returns:
            dict -- retruns the dict of the item that was searched for
        """
        _id = str(_id)
        if _id != None:
            if _id not in self.inventory.keys():
                raise KeyError(f"Item with ID: {_id} Does not exsist")
            return self.inventory[_id]
        elif name != None and _id == None:
            for k,v in self.inventory.items():
                if name in v.values():
                    return self.inventory[k]
            else:
                raise KeyError(f"No item was found with the name: {name}")
        else:
            raise ValueError("No argument given")

    def nextID(self) -> str:
        """
        Finds and return the next availeble ID in the database
        
        Returns:
            str -- returns the string version of next availeble ID that is not used
        """
        if not self.inventory:
            return "1"
        else:
            biggest = 0
            keyList = self.inventory.keys()
            for i in keyList:
                try:
                    i = int(i)
                except ValueError:
                    logging.exception(f"Found a invalid key {i}")
                if i > biggest:
                    biggest = i
            return str(biggest + 1)