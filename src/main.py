#!/usr/bin/python3
import os
import sys
import logging
import threading
import socket
import json

from config import *
import inventory as inv

logging.basicConfig(filename='srv.log', filemode='a', format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.DEBUG)

class Main:
    """
        The Main connection Between the socket, CLI and to the StockTracker
    """
    def __init__(self):
        self.tracker = inv.StockTracker()
        self.tracker.loadDB()
        self.inventory = self.tracker.inventory

    def addItem(self, name: str, description: str, amount: int):
        """
        Add Item to the inventory
        
        Arguments:
            name {str} -- [The name of the new item]
            description {str} -- [description of the new item]
            amount {int} -- [the amount of stock of the item]
        
        Returns:
            self -- retrun the values so it is availble outside of the scope
        """
        _id = self.tracker.nextID()
        item = inv.StockItem(_id, name, description, amount).addNewItem()
        self.inventory[_id] = item
        return self

    def editItem(self, _id: str, name: str, description: str, amount: int):
        """
        Edit an item in the inventory
        
        Arguments:
            _id {str} -- [The ID the user wants too edit]
            name {str} -- [The name of the new item]
            description {str} -- [description of the new item]
            amount {int} -- [the amount of stock of the item]
        
        Returns:
            self -- [description]
        """   
        item = inv.StockItem(_id, name, description, amount).updateItem()
        self.inventory[_id] = item
        return self

    def saveDB(self):
        self.tracker.saveDB()
        return self

    def exit(self):
        """Saves and exit the main thread
        """
        self.saveDB()
        sys.exit()
    

class Sock:
    """
        The Socket that used for connecting with clients

        Arguments:
            port {int} -- [The port that the socke should open for the server]

        Keyword Arguments:
            IPAddress {str} -- [The IP adddress that the socke should connect to] (default: {"0.0.0.0"})
    """
    def __init__(self, port: int,   IPAddress: str = "127.0.0.1"):
        self.IPAddress = IPAddress
        self.port = port
        self.headersize = 16
        self.cmdsize = 4
        self.openConnection()

    def openConnection(self):
        """Main thread in the socket connection
        """
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((self.IPAddress, self.port))
        s.listen(5)
        while True:
            clientsocket, address = s.accept()
            logging.info(f"A Client from {address} has connected")
            logging.debug(f"{clientsocket}, {address}")
            cmd = self.getCMD(clientsocket, address)
            cmdCode = cmd[self.headersize:self.headersize+self.cmdsize]
            data = cmd[self.headersize+self.cmdsize:]
            logging.debug(cmd)
            
            try:
                cmdCode = int(cmdCode)
            except Exception as e:
                logging.debug(e)

            # 1001 = New, 1002 = Edit, 1003 = Get spesific ID, 1004 = Get all items
            if cmdCode == 1001:
                item = json.load(data)
                logging.debug(item)
                main.addItem(item["name"], item["description"], item["amount"])
                self.sendData(clientsocket, address, "Item added")
                main.saveDB()
            elif cmdCode == 1002:
                item = json.load(data)
                logging.debug(item)
                main.editItem(item["id"],item["name"], item["description"], item["amount"])
                self.sendData(clientsocket, address, "Item edited")
                main.saveDB()
            elif cmdCode == 1003:
                try:
                    data = int(data)
                    item = main.tracker.getItem(data)
                    logging.debug(item)
                    self.sendData(clientsocket, address, item)
                except Exception:
                    self.sendData(clientsocket, address, "Something went wrong")
            elif cmdCode == 1004:
                self.sendData(clientsocket, address, json.dumps(main.inventory))

    def sendData(self, clientsocket, address, msg):
        """
        Sends the data to the client
        
        Arguments:
            clientsocket {socket} -- [Used to find the right connection]
            address {socket} -- [the address to the client]
            msg {str} -- [the data that will be sendt too the client]
        """
        msg = str(msg)
        msg_header = f"{len(msg):<{self.headersize}}" + msg
        clientsocket.send(bytes(msg_header,"utf-8"))
        logging.info(f"Data Sendt to client {address}")

    def getCMD(self, clientsocket, address):
        """[summary]
        
        Arguments:
            clientsocket {socket} -- [Used to find the right connection]
            address {socket} -- [the address to the client]
        
        Returns:
            str -- The full message that was recived from the server
        """
        full_cmd =  ""
        new_cmd = True
        while True:
            cmd = clientsocket.recv(self.headersize)
            if new_cmd:
                cmdlen = int(cmd[:self.headersize])
                new_cmd = False
            
            full_cmd += cmd.decode("utf-8")


            if len(full_cmd)-self.headersize == cmdlen:
                logging.info("New Command Recived")
                return full_cmd


class CLI:
    def __init__(self):
        self.menu()

    @property
    def menuText(self):
        text ="""
        -----------------------------------------
        ----------    Stock Manager    ----------
        -----------------------------------------
            1. Add new item to the inventory
            2. Edit item from inventory
            3. Remove item from inventory
            4. Show items in inventory
            5. exit
        -----------------------------------------
        """
        return text

    def clearScreen(self):
        os.system('cls' if os.name == 'nt' else 'clear')

    def menu(self):
        while True:
            print(self.menuText)
            self.action()

    def action(self):
        choice = input("> ")
        if choice == "":
            self.menu()
        else:
            try:
                choice = int(choice)
            except Exception:
                self.menu()
        if choice in range(1,6):
            self.validateInput(choice)
        else:
            print(f"{choice} is not an valid input")
            self.menu()

    def validateInput(self, choice: int):
        if choice == 1:
            item = self.getInput()
            main.addItem(item.name, item.description, item.amount)
            main.saveDB()
        elif choice == 2:
            _id = input("What item do you want to edit? \n(ID) > ")
            print(self.display(_id=_id))
            item = self.getInput()
            main.editItem(_id ,item.name, item.description, item.amount)
            main.saveDB()
        elif choice == 3:
            _id = input("What item do you want to remove? \n(ID) > ")
            print(f"{_id} won't be removed as it is not an implemented feature yet")
        elif choice == 4:
            print("What item do you want to show?")
            print("Show all by writeing \"all\"")
            _id = input("\n(ID) > ").lower()
            if _id == "all":
                print(self.display())
            else:
                print(self.display(_id=_id))
        elif choice == 5:
            print("Exiting inventory manager")
            sys.exit()

    def display(self, _id=None):
        if _id != None:
            try:
                item = main.tracker.getItem(_id)
            except Exception as e:
                logging.info(e)

            itemRow = f"""
            | ID |\t| Name |\t| Description |\t| Amount |
            | {item["id"]} |\t| {item["name"]} |\t| {item["description"]} |\t| {item["amount"]} |
            """
            return itemRow
        else:
            itemRow = f"""
            | ID |\t| Name |\t| Description |\t| Amount |\n
            """
            for e in main.inventory.values():
                item = e
                itemRow += f"""
                | {item["id"]} |\t| {item["name"]} |\t| {item["description"]} |\t| {item["amount"]} |\n
                """
            return itemRow


    def getInput(self):
        self.name = input("Name > ")
        self.description = input("Description > ")
        self.amount = input("Amount > ")
        return self
    
if __name__ == "__main__":
    logging.info("")
    logging.info("StockTracker is starting")
    main = Main()
    t1 = threading.Thread(target=Sock, args=(PORT, IP))
    t1.start()
    logging.info(f"Server started on port {PORT}")
    t2 = threading.Thread(target=CLI)
    t2.start()
    logging.info("CLI thread started")